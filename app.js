const store = require('./src/store');
const moment = require('moment');

const app = require('./src/api');
const port = process.env.NODE_PORT || 3220;
const server = app.listen(port, function() {
  console.log('file version control system running in port: ' + port);
});
