# Prueba cloner

El acercamiento utilizado es parecido a git  o a cuaquier "content addressed storage" (realizada utilizando NodeJS/ES6)

### layout archivos importantes

**/src/metadata.js**   # clase encargasa de manejar la metadata   
**/src/store.js**      # funciones de prueba solicitadas en el documento   
**/src/endpoint/endpointController.js**   # rutas de endpoints de prueba (modificar aca si quieres probar)  
**/test-files**        # archivos de prueba

Actualmente existen 2 estructuras:

### 1) fileStore:
hashtable que simula el almacenamiento final del contenido del archivo, puede ser otro servidor/AWS S3/base de datos etc...

### 2) objectStore:
hashtable donde cada instancia contiene la metadata de los archivos almacenados y tambien se encarga de mantener la jerarquia de las carpetas o cualquier otro "tree" (folder/subfolder etc...)


##### Ejemplo de objeto "file" metadata almacenada en "ObjectStore"

```json
{
   "702eac1e7d6779cf7aae2d7795d5340b1e8bbae0": {
      "name": "test1.txt",
      "path": "./files/test1.txt",
      "type": "file",
      "versions": {
         "cc32c92f52c7828c85e28785b648952816310d9d": {
            "created": [
               "1560463105"
            ],
            "content": "ec7c6b89f5f77d60645c7b1ff1723541135011f9"
         }
      }
   }
}
```

el primer hash corresponde al identificador unico, luego el se encuentran los campos descriptivos del objeto por ahora solo existen type "file" y "folder" los cuales mantienen la misma estructura y son instancias de la misma clase.

dentro de la key versions se encuentra almacenda la metadata de cada version, con el hash de su contenido para poder 'recuperar' el contenido desde filestore y un array de fechas 'created' para las cuales esa version es valida

##### Ejemplo de un objeto "file" con multiples versiones y multiples fechas validas para la primera version

```json
{
    "702eac1e7d6779cf7aae2d7795d5340b1e8bbae0": {
        "name": "test1.txt",
        "path": "./test-files/single-files-v1/test1.txt",
        "type": "file",
        "versions": {
            "d779091cf5dea16fa29cdf9f7709bd73ab59b29e": {
                "created": [
                    "1560463105",  "1560463106",  "1560463107"
                ],
                "content": "9362df4fcb770666f71783e808b1450bbead8336"
            },
            "aac5e4c7ad741fcb9c7a904347aa5527c24d32e2": {
                "created": [
                    "1560463109"
                ],
                "content": [
                    "7e4f9eef037dccd4e6d785dc6d0e0a50601969ca"
                ]
            }
        }
    }
}

```

##### Ejemplo de objeto folder con objetos "hijos"

```json
{
    "1c20fa76837ab290138c69e20aeb90922169f624": {
        "name": "Information.txt",
        "path": "test-files/folder/Information.txt",
        "type": "file",
        "versions": {
            "7eb69771a5fc7de10e26fb75fd63087135984aec": {
                "created": [
                    "1560463105"
                ],
                "content": "acb37cdaa6eaa39f2317c2f4677d4a729291b08d"
            }
        }
    },
    "86bd1d6614b125847f92578600b34e071af6df5e": {
        "name": "avatar.png",
        "path": "test-files/folder/avatar.png",
        "type": "file",
        "versions": {
            "172fcc5e9672e21723e6a53b432742032a4812cd": {
                "created": [
                    "1560463105"
                ],
                "content": "6e9e4d096dc033a78e004a8de7e8c6aff8b0e1b2"
            }
        }
    },
    "46f08070fd57eea90e2f3241ac90c96a7ac5c252": {
        "name": "notes.doc",
        "path": "test-files/folder/notes.doc",
        "type": "file",
        "versions": {
            "aab191021a7aec908d14029f8ab23009d1148f0a": {
                "created": [
                    "1560463105"
                ],
                "content": "efb76dffefc15d129de6c3de74ad16734b2aa5c0"
            }
        }
    },
    "4ae5fe97e6243903dc91bd4a1fca693a62be7657": {
        "name": "folder",
        "path": "./test-files/folder",
        "type": "folder",
        "versions": {
            "ef7f31aa127102ca9b9771bb7ea8a735ee675bec": {
                "created": [
                    "1560463105"
                ],
                "content": [
                    {
                        "object": "1c20fa76837ab290138c69e20aeb90922169f624",
                        "version": "7eb69771a5fc7de10e26fb75fd63087135984aec"
                    },
                    {
                        "object": "86bd1d6614b125847f92578600b34e071af6df5e",
                        "version": "172fcc5e9672e21723e6a53b432742032a4812cd"
                    },
                    {
                        "object": "46f08070fd57eea90e2f3241ac90c96a7ac5c252",
                        "version": "aab191021a7aec908d14029f8ab23009d1148f0a"
                    }
                ]
            }
        }
    }
}
```

con la metadata almacenada de esta forma (parecido a un version control system como git) no es necesario volver a guardar una version que ya tengo para una nueva fecha, solo agrego una nueva version valida en el array created y las keys al ser generadas a traves de un hash puedo conseguir los  identificadores sin tener que realizar busquedas complejas, ademas de ser posible replicar la estructura completa de un directorio para una determinada fecha y versión.

##### posibles mejoras ***

1) Muchas de los problemas son similares a los que resuelve un motor de base de datos asi que supongo que se estan ayudando por alguno

2) con un motor de base de datos se podrian probar otros acercamientos como el nested sets o utilizar una closure table junto con los timestamp de los archivos almacenados o uno que no me gusta mucho pero que igual lo he visto mucho en otras apps el ""adjacency-list""

3) Tambien no es necesario guardar los archivos completos como en esta prueba, se puede guardar el archivo por partes o almacenar solo los deltas (partiendo desde el archivo mas reciente y sus diferencias historicas para una busqueda mas rapida de las versiones recientes o partiendo desde la version inicial y sus diferencias hasta llegar a la version actual para una busqueda mas rapida de las versiones antiguas) desconozco el detalle de sus necesidades.


4) se podria utilizar el hash  generado a partir del contenido para distribuir su almacenamiento es distintos repositorios o comprmir este contenido con zlib o algun otro compresor, recordar que a los compresores les encantan la redundancia y al guardar archivos con solo "partes" diferentes estamos llenos de eso

5) prueba realizada utilisando sha1 para evitar "colisiones" del hash

6) algunos de los arrays utilizados en esta prueba podrian ser linkedlist en otros lenguajes, pero en ES6 tienen mucho mejor performance los arrays
(hasta tienen un sabor de loop para recorrerlos, buscar for.... of)


### algunos endpoints de prueba =======================================================

pueden utilizar algun navegador o curl para probar los siguientes endpoints que se encuentra montado en mi servidor de manera temporal:

#### simulación de agregar un nuevo archivo

[http://18.218.166.66:3220/api/cloner/addfile](http://18.218.166.66:3220/api/cloner/addfile)

#### simulación de agregar una nueva versión de un archivo

[http://18.218.166.66:3220/api/cloner/addversion](http://18.218.166.66:3220/api/cloner/addversion)

#### simulación de pruebas de archivos existentes

#### que si existe

[http://18.218.166.66:3220/api/cloner/exists-yep](http://18.218.166.66:3220/api/cloner/exists-yep)

#### que no existe

[http://18.218.166.66:3220/api/cloner/exists-nope](http://18.218.166.66:3220/api/cloner/exists-nope)


#### simulación de agregar una carpeta y su contenido

[http://18.218.166.66:3220/api/cloner/addfolder](http://18.218.166.66:3220/api/cloner/addfolder)


#### simulación de borrar una carpeta y su contenido

[http://18.218.166.66:3220/api/cloner/deletefolder](http://18.218.166.66:3220/api/cloner/deletefolder)

#### listar contenido de la carpeta en orden legixografico

[http://18.218.166.66:3220/api/cloner/listdir](http://18.218.166.66:3220/api/cloner/listdir)

#### mostrar versiones disponibles de un archivo para un rango de fechas

[http://18.218.166.66:3220/api/cloner/versionlist](http://18.218.166.66:3220/api/cloner/versionlist)
