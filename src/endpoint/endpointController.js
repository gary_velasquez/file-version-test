const store = require('../store');
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

//add test
router.get('/addfile', async function(req, res, next) {
  objectStore = {};
  await store.AddFile('./test-files/single-files-v1/test1.txt', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(JSON.stringify(objectStore, null, 4));
});

router.get('/addversion', async function(req, res, next) {
  objectStore = {};
  await store.AddFile('./test-files/single-files-v1/test1.txt', '1560463105');
  await store.AddFile('./test-files/single-files-v2/test1.txt', '1560463109');
  res.header("Content-Type", 'application/json');
  res.status(200).send(JSON.stringify(objectStore, null, 4));
});

//check test
router.get('/exists-yep', async function(req, res, next) {
  objectStore = {};
  await store.AddFile('./test-files/single-files-v1/test1.txt', '1560463105');
  const result = await store.Exists('./test-files/single-files-v1/test1.txt', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(result);
});

router.get('/exists-nope', async function(req, res, next) {
  objectStore = {};
  await store.AddFile('./test-files/single-files-v1/test1.txt', '1560463105');
  const result = await store.Exists('./test-files/single-files-v2/test1.txt', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(result);
});

//add folder
router.get('/addfolder', async function(req, res, next) {
  objectStore = {};
  await store.AddFolder('./test-files/folder', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(JSON.stringify(objectStore, null, 4));
});

//delete folder
router.get('/deletefolder', async function(req, res, next) {
  objectStore = {};
  await store.AddFolder('./test-files/folder', '1560463105');
  await store.DeleteFolder('./test-files/folder', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(JSON.stringify(objectStore, null, 4));
});

//list directory in order
router.get('/listdir', async function(req, res, next) {
  objectStore = {};
  await store.AddFolder('./test-files/folder', '1560463105');
  const result = await store.ListDir('./test-files/folder', '1560463105');
  res.header("Content-Type", 'application/json');
  res.status(200).send(result);
});

//version list
router.get('/versionlist', async function(req, res, next) {
  objectStore = {};
  await store.AddFile('./test-files/single-files-v1/test1.txt', '1560463105');
  await store.AddFile('./test-files/single-files-v2/test1.txt', '1560463109');
  //const version = await store.VersionList('./files/test1.txt', '1560463103', '1560463107');
  const version = await store.VersionList('./files/test1.txt', '1560463103', '1560463111');
  res.header("Content-Type", 'application/json');
  res.status(200).send(JSON.stringify(version, null, 4));
});

module.exports = router;
