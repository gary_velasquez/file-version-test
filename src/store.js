//metadata store
global.objectStore = {};
const hash = require('./utils/hash');
const buffer = require('./utils/buffer');
const fileReader = require('./utils/fileReader');
const dateHelper = require('./utils/date');
const MetaData = require('./metadata');
//file content store
const fileStore = {};
module.exports = {
  AddFile: async (path, created) => {
    try {
      //proceso el contenido del archivo
      const versionHASH = await uploadFileContent(path, true);
      const metadata = new MetaData(path, 'file');
      //reviso si ya tengo metadata sobre este objeto tipo 'file'
      if (metadata.objectExists(objectStore)) {
        //reviso si ya tengo esta versión del objeto que estoy tratando de subir
        if (metadata.versionExists(versionHASH, objectStore)) {
          //reviso si ya tengo la fecha para esta versión
          if (metadata.dateExists(versionHASH, created, objectStore)) {
            //ya tengo almacenada esta objeto, version y fecha correctamente almacenado, no tengo que hacer nada mas
          } else {
            //agrego una nueva fecha valida para este objeto
            metadata.addNewDate(versionHASH, created, objectStore);
          }
        } else {
          //agrego una nueva version para este objeto
          metadata.addNewVersion(versionHASH, created, objectStore);
        }
      } else {
        //objeto completamente nuevo, creo primera version
        metadata.addNewObject(versionHASH, created, objectStore);
      }
      return {object: metadata.elementID, version: metadata.getVersionID(versionHASH)};
    } catch (e) {
      //'cheap and useless error handling...
      console.log(e);
    }
  },
  Exists: async (path, when) => {
    const versionHASH = await uploadFileContent(path, false);
    const metadata = new MetaData(path, 'file');
    if (metadata.objectExists(objectStore)) {
      if (metadata.versionExists(versionHASH, objectStore)) {
        if (metadata.dateExists(versionHASH, when, objectStore)) {
          return true;
        }
      }
    }
    return false;
  },
  DeleteFile: async (path, deleted) => {
    //borra logicamente la version del archivo para una determinada fecha
    if (await module.exports.Exists(path, deleted)) {
      const versionHASH = await uploadFileContent(path, false);
      const metadata = new MetaData(path, 'file');
      metadata.deleteDate(versionHASH, deleted, objectStore);
    } else {
      console.log('file not found');
    }
  },
  AddFolder: async (path, created) => {
    const versionArray = [];
    //leo recursivamente el directorio y agrego cada uno de sus archivos como objetos tipo "file"
    const fileArray = await fileReader.readFolder(path);
    for (let file of fileArray) {
      const metadata = await module.exports.AddFile(file, created);
      versionArray.push(metadata);
    }
    //finalmente agrego el objeto tipo "folder"
    const metadata = new MetaData(path, 'folder');
    metadata.addNewObject(versionArray, created, objectStore);
  },
  DeleteFolder: async (path, deleted) => {;
    const versionArray = [];
    const fileArray = await fileReader.readFolder(path);
    //busco los "childrens" del objeto tipo 'folder' y sus versiones correspondientes
    for (let file of fileArray) {
      const versionHASH = await uploadFileContent(file, false);
      const metadata = new MetaData(file, 'file');
      const foundObject = metadata.getObject(versionHASH);
      versionArray.push(foundObject);
    }
    //borro de manera logica cada uno de estas versiones y finalmente el objeto "padre"
    const metadata = new MetaData(path, 'folder');
    metadata.deleteChildren(versionArray, deleted, objectStore);
  },
  ListDir: async (path, when) => {
    const versionArray = [];
    const fileArray = await fileReader.readFolder(path);
    for (let file of fileArray) {
      //busco los "childrens" del objeto tipo 'folder' y sus versiones correspondientes
      const versionHASH = await uploadFileContent(file, false);
      const metadata = new MetaData(file, 'file');
      const foundObject = metadata.getObject(versionHASH);
      versionArray.push(foundObject);
    }
    //consigo todos los chidlren que cumplen con la condicion de fecha para esa instancia
    const metadata = new MetaData(path, 'folder');
    return metadata.getChildren(versionArray, when, objectStore);
  },
  VersionList: async (path, start, end) => {
    const resultArray = [];
    const metadata = new MetaData(path, 'file');
    if (metadata.objectExists(objectStore)) {
      const currentVersions = metadata.getVersion(objectStore);
      for (version in currentVersions) {
        //reviso los objetos que cumplen con la condición de fecha
        if (dateHelper.checkDate(currentVersions[version]['created'], start, end)) {
          resultArray.push(currentVersions[version]);
        }
      }
    }
    return resultArray;
  }
}

//simulación de upload del contenido a (database/filestorage/aws-s3, etc...)
async function uploadFileContent(path, upload) {
  const fileContent = await buffer.getBuffer(path);
  //genero el identificador de la version
  const contentHash = hash.getHash(fileContent);
  if (upload) {
    fileStore[contentHash] = fileContent;
  }
  return contentHash;
}
