const fs = require('fs');
const crypto = require('crypto');

module.exports = {
  getFileHash: async (filepath) => {
    return new Promise(function(resolve, reject) {
      const hash = crypto.createHash('sha1')
      const stream = fs.createReadStream(filepath);
      stream.on('data', function(data) {
        hash.update(data, 'utf8')
      })
      stream.on('end', function() {
        resolve(hash.digest('hex'));
      })
    })
  },
  getHash: function(element) {
    const hash = crypto.createHash('sha1');
    hash.update(JSON.stringify(element));
    return hash.digest('hex');
  }
}
