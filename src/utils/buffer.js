const fs = require('fs');
module.exports = {
  getBuffer: function(filepath) {
    return new Promise(function(resolve, reject) {
      fs.readFile(filepath, (err, data) => {
        err
          ? reject(err)
          : resolve(data);
      });
    });
  }
}
