const moment = require('moment');
module.exports = {
  checkDate: function(array, stardate, endDate) {
    for (let date of array){
      return moment.unix(date).isBetween(moment.unix(stardate), moment.unix(endDate));
    }
  }
}
