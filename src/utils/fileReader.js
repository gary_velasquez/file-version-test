const fs = require('fs');
const path = require('path');
module.exports = {
  readFolder: async (dir, filelist = [], fileDirectory = []) => {
    const files = await fs.readdirSync(dir);
    for (file of files) {
      const filepath = path.join(dir, file);
      const stat = await fs.statSync(filepath);
      if (stat.isDirectory()) {
        fileDirectory.push(filepath);
        filelist = await readFolder(filepath, filelist);
      } else {
        filelist.push(filepath);
      }
    }
    return filelist;
  },
  getFilename: function(dir) {
    return path.basename(dir);
  }
}
