const hash = require('./utils/hash');
const buffer = require('./utils/buffer');
const fileReader = require('./utils/fileReader');

class MetaData {
  constructor(path, objectType) {
    const element = {
      type: objectType,
      name: fileReader.getFilename(path)
    };
    this.elementID = hash.getHash(element);
    this.type = objectType;
    this.name = element.name;
    this.path = path;
  }
  getVersionID(versionHASH) {
    return hash.getHash({object: this.elementID, version: versionHASH});
  }
  getObject(versionHASH) {
    return {object: this.elementID, version: this.getVersionID(versionHASH)}
  }
  addNewObject(versionHASH, created, objectStore) {
    const element = {}
    if (Array.isArray(versionHASH)) {
      var versionID = this.getArrayVersionID(versionHASH);
    } else {
      var versionID = this.getVersionID(versionHASH);
    }
    element['name'] = this.name;
    element['path'] = this.path;
    element['type'] = this.type;
    element['versions'] = {
      [versionID]: {
        created: [created],
        content: versionHASH
      }
    }
    objectStore[this.elementID] = element;
  }
  objectExists(objectStore) {
    if (this.elementID in objectStore) {
      return true
    }
    return false;
  }
  versionExists(versionHASH, objectStore) {
    const versionID = this.getVersionID(versionHASH);
    if (objectStore[this.elementID]['versions'][versionID] !== undefined) {
      return true;
    }
    return false;
  }
  dateExists(versionHASH, created, objectStore) {
    const versionID = this.getVersionID(versionHASH);
    if (objectStore[this.elementID]['versions'][versionID]['created'].indexOf(created) === -1) {
      return false;
    } else {
      return true;
    }
  }
  addNewDate(versionHASH, created, objectStore) {
    const versionID = this.getVersionID(versionHASH);
    objectStore[this.elementID]['versions'][versionID]['created'].push(created);
    return versionID;
  }
  addNewVersion(versionHASH, created, objectStore) {
    const versionID = this.getVersionID(versionHASH);
    objectStore[this.elementID]['versions'][versionID] = {
      created: [created],
      content: [versionHASH]
    }
  }
  deleteDate(versionHASH, deleted, objectStore) {
    const versionID = this.getVersionID(versionHASH);
    let current = objectStore[this.elementID]['versions'][versionID]['created'];
    current.splice(current.indexOf(deleted), 1);
  }
  getArrayVersionID(childrenArray) {
    return hash.getHash(childrenArray);
  }
  deleteChildren(versionHASH, deleted, objectStore) {
    var versionID = this.getArrayVersionID(versionHASH);
    let current = objectStore[this.elementID]['versions'][versionID]
    if (current['created'].indexOf(deleted) > -1) {
      //utilizo la referencia para borrar cada archivo o directorio que actualmente es parte de esta version
      for (let contentKey in current['content']) {
        let contentID = current['content'][contentKey];
        delete objectStore[contentID['object']];
      }
    }
    //una vez que no tene mas "hijos" finalmente borro la version del elemento padre el cual queda vacio
    //podria existir un proceso de "garbage collection" o limpieza posterior para no sobre complejizar operaciones atomicas
    delete objectStore[this.elementID]['versions'][versionID];
  }
  getChildren(versionHASH, when, objectStore) {
    const resultArray = [];
    var versionID = this.getArrayVersionID(versionHASH);
    let current = objectStore[this.elementID]['versions'][versionID];
    if (current['created'].indexOf(when) > -1) {
      for (let contentKey in current['content']) {
        let contentID = current['content'][contentKey];
        resultArray.push(objectStore[contentID['object']]['name']);
      }
    }
    //orden lexicografico
    return resultArray.sort();
  }
  getVersion(objectStore) {
    return objectStore[this.elementID]['versions'];
  }
}

module.exports = MetaData;
