var express = require('express');
var app = express();

app.get('/api', function(req, res) {
  res.status(200).send('API works.');
});

var EndpointController = require( './endpoint/endpointController');
app.use('/api/cloner', EndpointController);

module.exports = app;
